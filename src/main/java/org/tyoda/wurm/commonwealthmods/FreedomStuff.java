/*
KingdomSails mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.commonwealthmods;

import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.items.*;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;

import java.io.IOException;
import java.util.logging.Level;

public class FreedomStuff {
    public static void addFreedomStuff(){
        String name = "Freedom";
        String model = "model.decoration.flag.free";
        ItemTemplateBuilder builder = new ItemTemplateBuilder("org.kingdom.flag." + name)
            .name(name + " flag", name + " flags", "A symbol of the " + name + " kingdom.")
            .descriptions("excellent", "good", "ok", "poor")
            .itemTypes(new short[]{
                ItemTypes.ITEM_TYPE_CLOTH,
                ItemTypes.ITEM_TYPE_TURNABLE,
                ItemTypes.ITEM_TYPE_COLORABLE,
                ItemTypes.ITEM_TYPE_WIND,
                ItemTypes.ITEM_TYPE_MISSION,
                ItemTypes.ITEM_TYPE_DECORATION,
                ItemTypes.ITEM_TYPE_FOUR_PER_TILE,
                ItemTypes.ITEM_TYPE_HASDATA,
                ItemTypes.ITEM_TYPE_DESTROYABLE,
                ItemTypes.ITEM_TYPE_IMPROVEITEM,
                ItemTypes.ITEM_TYPE_REPAIRABLE,
                ItemTypes.ITEM_TYPE_PLANTABLE,
                ItemTypes.ITEM_TYPE_IMPROVE_USES_TYPE_AS_MATERIAL
            })
            .imageNumber((short) 640)
            .combatDamage(0)
            .decayTime(9072000L)
            .dimensions(5, 5, 205)
            .primarySkill(-10)
            .modelName(model + ".")
            .difficulty(40.0f)
            .weightGrams(2500)
            .material(Materials.MATERIAL_WOOD_BIRCH)
            .value(10000)
            .isTraded(true)
            .behaviourType(BehaviourList.itemBehaviour);
        ItemTemplate result = null;
        try {
            result = builder.build();
        }catch (IOException e){
            CommonwealthMods.logger.log(Level.SEVERE, "Failed while making item template.", e);
        }
        if(result != null) {
            CreationEntryCreator.createSimpleEntry(10016, 213, 23, result.getTemplateId(), true, true, 0.0F, false, false, CreationCategories.FLAGS);
        }

        addBanner(false);
        addBanner(true);
    }

    private static void addBanner(boolean tall){
        String name = "Freedom";
        String itemId = "org.kingdom.banner.";
        if (tall) {
            itemId += "tall.";
        }
        ItemTemplateBuilder builder = new ItemTemplateBuilder(itemId + name);
        String add = "";
        if (tall) {
            add = " tall";
        }
        builder.name(name + add + " banner", name + add + " banners", "An elegant symbol of allegiance and faith towards the " + name)
            .descriptions("excellent", "good", "ok", "poor")
            .itemTypes(new short[]{
                    ItemTypes.ITEM_TYPE_CLOTH,
                    ItemTypes.ITEM_TYPE_COLORABLE,
                    ItemTypes.ITEM_TYPE_MISSION,
                    ItemTypes.ITEM_TYPE_TURNABLE,
                    ItemTypes.ITEM_TYPE_DECORATION,
                    ItemTypes.ITEM_TYPE_FOUR_PER_TILE,
                    ItemTypes.ITEM_TYPE_HASDATA,
                    ItemTypes.ITEM_TYPE_DESTROYABLE,
                    ItemTypes.ITEM_TYPE_IMPROVEITEM,
                    ItemTypes.ITEM_TYPE_REPAIRABLE,
                    ItemTypes.ITEM_TYPE_PLANTABLE,
                    ItemTypes.ITEM_TYPE_IMPROVE_USES_TYPE_AS_MATERIAL
            })
            .imageNumber((short) 640)
            .combatDamage(0)
            .decayTime(9072000L)
            .dimensions(5, 5, 205)
            .primarySkill(-10)
            .modelName(tall ? "model.decoration.tallbanner.free." : "model.decoration.banner.free.")
            .difficulty(40.0f)
            .weightGrams(2500)
            .material(Materials.MATERIAL_WOOD_BIRCH)
            .value(10000)
            .isTraded(true)
            .behaviourType(BehaviourList.itemBehaviour);
        ItemTemplate result = null;

        try{
            result = builder.build();
        } catch (IOException e){
            CommonwealthMods.logger.log(Level.SEVERE, "Failed while making item template.", e);
        }
        if(result != null) {
            CreationEntryCreator.createSimpleEntry(10016, 213, 23, result.getTemplateId(), true, true, 0.0F, false, false, CreationCategories.FLAGS);
        }
    }
}
