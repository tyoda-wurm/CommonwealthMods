/*
KingdomSails mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.commonwealthmods;

import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.items.*;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.IconConstants;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;

import java.io.IOException;
import java.util.logging.Level;

public class Toyot {
    public static int createStatuette(){
        String modelName = "mod.tyoda.commonwealthmods.statuette.toyot";
        ItemTemplateBuilder builder = new ItemTemplateBuilder(modelName)
            .name("statuette of Toyot", "statuettes", "A statuette resembling the artist's interpretation of the mighty Toyot.")
            .itemTypes(new short[]{
                    ItemTypes.ITEM_TYPE_NAMED,
                    ItemTypes.ITEM_TYPE_DECORATION,
                    ItemTypes.ITEM_TYPE_METAL,
                    ItemTypes.ITEM_TYPE_REPAIRABLE,
                    ItemTypes.ITEM_TYPE_MATERIAL_PRICEEFFECT,
                    ItemTypes.ITEM_TYPE_COLORABLE,
                    ItemTypes.ITEM_TYPE_MISSION
            })
            .imageNumber((short) IconConstants.ICON_DECO_STATUETTE)
            .behaviourType(BehaviourList.itemBehaviour)
            .decayTime(2419200L)
            .modelName(modelName+".")
            .dimensions(3, 5, 20)
            .primarySkill(-10)
            .bodySpaces(new byte[0])
            .difficulty(40)
            .weightGrams(1000)
            .material(Materials.MATERIAL_UNDEFINED)
            .value(20000);

        ItemTemplate template = null;
        try {
            template = builder.build();
        } catch (IOException e){
            CommonwealthMods.logger.log(Level.SEVERE, "Failed while creating item template.", e);
        }

        if(template != null){
            CreationEntryCreator.createSimpleEntry(SkillList.SMITHING_GOLDSMITHING, ItemList.anvilSmall, ItemList.goldBar,     template.getTemplateId(), false, true, 0.0F, false, false, CreationCategories.STATUETTES);
            CreationEntryCreator.createSimpleEntry(SkillList.SMITHING_GOLDSMITHING, ItemList.anvilSmall, ItemList.silverBar,   template.getTemplateId(), false, true, 0.0F, false, false, CreationCategories.STATUETTES);
            CreationEntryCreator.createSimpleEntry(SkillList.SMITHING_GOLDSMITHING, ItemList.anvilSmall, ItemList.electrumBar, template.getTemplateId(), false, true, 0.0F, false, false, CreationCategories.STATUETTES);
            return template.getTemplateId();
        }
        return -10;
    }
}
