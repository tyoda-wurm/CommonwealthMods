/*
KingdomSails mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.commonwealthmods;

import com.wurmonline.server.Players;
import com.wurmonline.server.ServerEntry;
import com.wurmonline.server.Servers;
import com.wurmonline.server.players.Player;
import javassist.*;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.*;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;
import org.gotti.wurmunlimited.modsupport.items.ModItems;
import org.tyoda.wurm.commonwealthmods.xmtt.XmasTwentyThree;
import org.tyoda.wurm.commonwealthmods.xmtt.XmasTwentyThreeAction;
import org.tyoda.wurm.commonwealthmods.xmtt.XmasTwentyThreeBehaviour;

import java.util.HashSet;
import java.util.logging.Logger;

public class CommonwealthMods implements WurmServerMod, PreInitable, ItemTemplatesCreatedListener, ServerStartedListener {
    public static final Logger logger = Logger.getLogger(CommonwealthMods.class.getName());
    public static final String version = "0.6";

    @Override
    public void preInit(){
        ModItems.init();
        ModActions.init();
        try {
            CtClass ctLoginHandler = HookManager.getInstance().getClassPool().get("com.wurmonline.server.LoginHandler");
            CtMethod ctSendWho = ctLoginHandler.getDeclaredMethod("sendWho");
            logger.info("Starting injection of sendWho");
            ctSendWho.instrument(new ExprEditor() {
                private int sendSafeServerMessageCount = 0;
                @Override
                public void edit(MethodCall m) throws CannotCompileException {
                    if(m.getClassName().equals("com.wurmonline.server.creatures.Communicator")
                            && m.getMethodName().equals("sendSafeServerMessage")
                            && ++sendSafeServerMessageCount == 6){
                        logger.info("injecting sendWho");
                        m.replace("{" +
                            "int otherPlayers = org.tyoda.wurm.commonwealthmods.CommonwealthMods.getPlayersOnline() - 1;" +
                            "if(otherPlayers == 0){" +
                                "$1 = \"Welcome to The Commonwealth! No other players are online.\";" +
                            "}else if(otherPlayers == 1){" +
                                "$1 = \"Welcome to The Commonwealth! 1 other player is online.\";" +
                            "}else{" +
                                "$1 = \"Welcome to The Commonwealth! \" + otherPlayers + \" other players are online.\";" +
                            "}" +
                            "$_ = $proceed($$);" +
                        "}");
                    }
                }
            });
            logger.info("Done with injection of sendWho");
        } catch (NotFoundException | CannotCompileException e) {
            logger.severe("Failed while injecting bytecode");
            throw new HookException(e);
        }
    }

    @Override
    public void onItemTemplatesCreated() {
        FreedomStuff.addFreedomStuff();
        Toyot.createStatuette();

        XmasTwentyThreeBehaviour.setGiftId(XmasTwentyThree.createGiftTemplate());
        XmasTwentyThreeAction woodenElephantAction = new XmasTwentyThreeAction("Wooden elephant", XmasTwentyThree.createElephantTemplate());
        XmasTwentyThreeAction horseStatuetteAction = new XmasTwentyThreeAction("Horse statuette", XmasTwentyThree.createHorseTemplate());
        XmasTwentyThreeBehaviour.addOpenableAction(woodenElephantAction.getActionEntry());
        XmasTwentyThreeBehaviour.addOpenableAction(horseStatuetteAction.getActionEntry());
        ModActions.registerAction(woodenElephantAction);
        ModActions.registerAction(horseStatuetteAction);
        ModActions.registerAction(new XmasTwentyThreeBehaviour());
    }

    @Override
    public void onServerStarted(){

    }

    @Override
    public String getVersion(){
        return version;
    }

    @SuppressWarnings("unused")
    public static int getPlayersOnline(){
        int currentPlayers = 0;
        for (ServerEntry server : Servers.getAllServers()) {
            if (server.id != com.wurmonline.server.Servers.localServer.id)
                currentPlayers += server.currentPlayers;
        }

        Player[] players = Players.getInstance().getPlayers();

        HashSet<Long> ids = new HashSet<>((int) (players.length * 1.25) + 1);

        for (Player player : players) {
            ids.add(player.getSaveFile().getSteamId().getSteamID64());
        }
        currentPlayers += ids.size();

        return currentPlayers;
    }

    // TODO: different stages for planters
}
