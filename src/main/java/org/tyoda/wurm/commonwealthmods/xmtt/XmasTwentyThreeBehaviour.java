/*
KingdomSails mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.commonwealthmods.xmtt;

import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import org.gotti.wurmunlimited.modsupport.actions.BehaviourProvider;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;

import java.util.ArrayList;
import java.util.List;

public class XmasTwentyThreeBehaviour implements ModAction, BehaviourProvider {
    private static final List<ActionEntry> openAbleActions = new ArrayList<>();

    private static int giftId = -10;

    public static void addOpenableAction(ActionEntry actionEntry) {
        openAbleActions.add(actionEntry);
    }

    public static void setGiftId(int newGiftId) {
        giftId = newGiftId;
    }

    public List<ActionEntry> getBehavioursFor(Creature performer, Item target) {
        return getBehaviours(performer, target);
    }

    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Item target) {
        return getBehaviours(performer, target);
    }

    public List<ActionEntry> getBehaviours(Creature performer, Item target) {
        List<ActionEntry> list = null;
        if(!openAbleActions.isEmpty() && validate(performer, target)){
            list = new ArrayList<>(openAbleActions);
            list.add(0, new ActionEntry((short)-list.size(), "Open", "Opening"));
        }

        return list;
    }

    public static boolean validate(Creature performer, Item target) {
        return performer instanceof Player
                && target != null
                && target.getTemplateId() == giftId
                && performer.getInventory().getItems().contains(target);
    }
}
