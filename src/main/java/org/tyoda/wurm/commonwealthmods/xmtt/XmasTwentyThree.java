/*
KingdomSails mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.commonwealthmods.xmtt;

import com.wurmonline.server.TimeConstants;
import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.items.*;
import com.wurmonline.shared.constants.IconConstants;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;
import org.tyoda.wurm.commonwealthmods.CommonwealthMods;

import java.io.IOException;
import java.util.logging.Level;

public class XmasTwentyThree {
    public static int createGiftTemplate(){
        String modelName = "model.container.giftbox.holly.";
        ItemTemplateBuilder builder = new ItemTemplateBuilder(modelName)
                .name("gift", "gifts", "A carefully packaged gift. You think you might be able to unbox one of two different items from it.")
                .itemTypes(new short[]{
                        ItemTypes.ITEM_TYPE_NAMED,
                        ItemTypes.ITEM_TYPE_DECORATION
                })
                .imageNumber((short) IconConstants.ICON_CONTAINER_GIFT)
                .behaviourType(BehaviourList.itemBehaviour)
                .decayTime(TimeConstants.DECAYTIME_NEVER)
                .modelName(modelName)
                .dimensions(20, 20, 20)
                .primarySkill(-10)
                .bodySpaces(new byte[0])
                .difficulty(99)
                .weightGrams(2250)
                .material(Materials.MATERIAL_PAPER)
                .value(20000);

        try {
            ItemTemplate template = builder.build();
            return template.getTemplateId();
        } catch (IOException e){
            CommonwealthMods.logger.log(Level.SEVERE, "Failed while creating item template.", e);
        }

        return -10;
    }

    public static int createElephantTemplate(){
        String modelName = "model.mod.tyoda.commonwealthmods.decoration.elephant.";
        ItemTemplateBuilder builder = new ItemTemplateBuilder(modelName)
                .name("wooden elephant", "elephants", "A carefully carved wooden elephant.")
                .itemTypes(new short[]{
                        ItemTypes.ITEM_TYPE_NAMED,
                        ItemTypes.ITEM_TYPE_DECORATION,
                        ItemTypes.ITEM_TYPE_WOOD
                })
                .imageNumber((short) IconConstants.ICON_DECO_STATUETTE)
                .behaviourType(BehaviourList.itemBehaviour)
                .decayTime(TimeConstants.DECAYTIME_NEVER)
                .modelName(modelName)
                .dimensions(5, 10, 20)
                .primarySkill(-10)
                .bodySpaces(new byte[0])
                .difficulty(99)
                .weightGrams(750)
                .material(Materials.MATERIAL_WOOD_LINDEN)
                .value(20000);

        try {
            ItemTemplate template = builder.build();
            return template.getTemplateId();
        } catch (IOException e){
            CommonwealthMods.logger.log(Level.SEVERE, "Failed while creating item template.", e);
        }

        return -10;
    }

    public static int createHorseTemplate() {
        String modelName = "model.mod.tyoda.commonwealthmods.decoration.horseStatuette.";
        ItemTemplateBuilder builder = new ItemTemplateBuilder(modelName)
                .name("horse statuette", "statuettes", "The figure of a majestic horse carved out of marble.")
                .itemTypes(new short[]{
                        ItemTypes.ITEM_TYPE_NAMED,
                        ItemTypes.ITEM_TYPE_DECORATION,
                        ItemTypes.ITEM_TYPE_STONE
                })
                .imageNumber((short) IconConstants.ICON_DECO_STATUETTE)
                .behaviourType(BehaviourList.itemBehaviour)
                .decayTime(TimeConstants.DECAYTIME_NEVER)
                .modelName(modelName)
                .dimensions(5, 5, 20)
                .primarySkill(-10)
                .bodySpaces(new byte[0])
                .difficulty(99)
                .weightGrams(1500)
                .material(Materials.MATERIAL_MARBLE)
                .value(20000);

        try {
            ItemTemplate template = builder.build();
            return template.getTemplateId();
        } catch (IOException e){
            CommonwealthMods.logger.log(Level.SEVERE, "Failed while creating item template.", e);
        }

        return -10;
    }
}
