/*
KingdomSails mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.commonwealthmods.xmtt;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.Items;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.*;
import org.gotti.wurmunlimited.modsupport.actions.ActionPerformer;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;
import org.tyoda.wurm.commonwealthmods.CommonwealthMods;

import java.util.logging.Level;


public class XmasTwentyThreeAction implements ModAction, ActionPerformer {
    private final short actionId = (short) ModActions.getNextActionId();
    private final ActionEntry actionEntry;

    private final int itemId;

    public XmasTwentyThreeAction(String actionName, int itemId) {
        this.itemId = itemId;
        this.actionEntry = ActionEntry.createEntry(this.actionId, actionName, "opening gift", new int[0]);
        ModActions.registerAction(this.actionEntry);
    }

    public ActionEntry getActionEntry() {
        return actionEntry;
    }

    public short getActionId() {
        return this.actionId;
    }

    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        return doAction(action, performer, target, counter);
    }

    public boolean action(Action action, Creature performer, Item target, short num, float counter) {
        return doAction(action, performer, target, counter);
    }

    private boolean doAction(Action action, Creature performer, Item target, float counter){
        if (counter == 1.0f) {
            if(!XmasTwentyThreeBehaviour.validate(performer, target))
                return true;
            int time = 100;
            action.setTimeLeft(time);
            performer.sendActionControl("Opening gift", true, time);
            performer.getCommunicator().sendNormalServerMessage("You start to open the gift.");
            Server.getInstance().broadCastAction(performer.getName() + " starts to open a gift.", performer, 5);
            CommonwealthMods.logger.log(Level.INFO, performer.getName() + " starts to open a 23 gift as "+actionEntry.getActionString()+'.');
        }
        else {
            int time = action.getTimeLeft();

            if (counter * 10 > time) {
                try {
                    Item newItem = ItemFactory.createItem(itemId, 90.0f, (byte) 0, null);
                    performer.getInventory().insertItem(newItem, true);
                    performer.getCommunicator().sendNormalServerMessage("You find a decorative item inside the box.");
                    Items.destroyItem(target.getWurmId(), false, false);
                    performer.getCommunicator().sendNormalServerMessage("You discard the wrapping paper.");
                } catch (FailedException | NoSuchTemplateException e) {
                    CommonwealthMods.logger.log(Level.SEVERE, "Failed to open gift for " + performer.getName());
                    performer.getCommunicator().sendNormalServerMessage("You are confused by the box and decide not to open it.");
                }
                return true;
            }
        }
        return false;
    }
}
